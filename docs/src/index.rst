SKA Dishmaster Simulator's Documentation
========================================

.. automodule:: ska_sim_dishmaster

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
