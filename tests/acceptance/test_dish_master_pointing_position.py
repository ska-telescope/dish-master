"""
BDD tests for dish master pointing
"""

import logging
import time

import pytest
from assertpy import assert_that
from pytest_bdd import given, parsers, scenario, then, when
from tango import DeviceProxy

from tests.acceptance.utils import (
    dish_mode_pre_condition,
    request_mode_change,
    retrieve_attr_value,
    wait_for_change_on_resource,
)

LOGGER = logging.getLogger(__name__)

# pylint: disable=too-many-locals

# constants are from the dish behaviour
# Use elevation to estimate time expected to arrive on target since
# it moves slower
MAX_DESIRED_AZIM = 270.0
MAX_DESIRED_ELEV = 90.0
ELEV_DRIVE_MAX_RATE = 1.0
TOL = 5e-3  # found in almost_equal function


@pytest.fixture(scope="module", name="dish_proxy")
def fixture_dish_proxy():
    # pylint: disable=missing-function-docstring
    return DeviceProxy("mid_d0001/elt/master")


@pytest.fixture(autouse=True, scope="module")
def restore_dish_state(request, dish_proxy):
    """A teardown function which will ensure that the dish is restored to
    STANDBY state
    """

    def put_dish_in_standby_fp_mode():
        LOGGER.info("Restoring dish_master to STANDBY state")
        request_mode_change(dish_proxy, "STANDBY_FP")

    request.addfinalizer(put_dish_in_standby_fp_mode)


@scenario("dish-pointing.feature", "Test dish pointing request")
def test_dish_pointing():
    # pylint: disable=missing-function-docstring
    pass


@given(parsers.parse("dish master reports {allowed_mode} dishMode"))
def dish_reports_allowed_dish_mode(dish_proxy, allowed_mode):
    # pylint: disable=missing-function-docstring
    dish_mode_pre_condition(dish_proxy, allowed_mode)
    LOGGER.info(f"dish_master current dishMode: {allowed_mode}")


@given(
    parsers.parse("dish master reports {allowed_pointing_state} pointingState")
)
def dish_reports_allowed_pointing_state(dish_proxy, allowed_pointing_state):
    # pylint: disable=missing-function-docstring
    current_pointing_state = retrieve_attr_value(dish_proxy, "pointingState")
    assert_that(current_pointing_state).is_equal_to(allowed_pointing_state)
    LOGGER.info(f"dish master current pointingState: {current_pointing_state}")


@when(
    parsers.parse(
        "I request the azimuth and elevation to move by {delta:d} degrees each"
    )
)
def request_pointing_position(dish_proxy, delta):
    # pylint: disable=missing-function-docstring

    # determine the az, el coordinates
    az, el = dish_proxy.achievedPointing[1:]
    # azimuth
    if az + delta >= MAX_DESIRED_AZIM:
        az -= delta
    else:
        az += delta
    # elevation
    if el + delta >= MAX_DESIRED_ELEV:
        el -= delta
    else:
        el += delta

    # the az,el coordinates sent to dish master must have a
    # future timestamp in order for them to be considered
    cmd_time_offset = 5000  # milliseconds
    dish_proxy.desiredPointing = [
        time.time() * 1000.0 + cmd_time_offset,
        az,
        el,
    ]
    dish_proxy.Track()  # transitions to SLEW pointingState

    # wait a while for pre-update method to be triggered.
    # pointingState transitions from SLEW to TRACK while waiting
    time.sleep(1.5)

    # current dish behaviour uses internal pointing variables to determine
    # if dish is on target. since those values get updated a little late,
    # dish reports target is locked and transitions pointingState to TRACK.
    # Wait a while for the internal attribute to update targetLock to False
    # after the desiredPointing is used and then transition to SLEW again
    wait_for_change_on_resource(dish_proxy, "targetLock", True, timeout=6)
    dish_proxy.Track()
    # wait a while for pointingState to transition from TRACK to SLEW
    wait_for_change_on_resource(dish_proxy, "pointingState", "TRACK")
    current_pointing_state = retrieve_attr_value(dish_proxy, "pointingState")
    assert_that(current_pointing_state).is_equal_to("SLEW")

    el_delta = abs(el - dish_proxy.desiredPointing[2])
    expected_time_to_move = el_delta / ELEV_DRIVE_MAX_RATE
    # add an arbitrary 10s el time tolerance
    future_time = time.time() + expected_time_to_move + 10
    dish_far_from_requested_position = True

    while dish_far_from_requested_position:
        # keep sending the coordinates every 200ms spaced at the offset
        dish_proxy.desiredPointing = [
            time.time() * 1000.0 + cmd_time_offset,
            az,
            el,
        ]
        time.sleep(0.2)
        # calculate how far dish is
        desired_az, achieved_az = (
            dish_proxy.desiredPointing[1],
            dish_proxy.achievedPointing[1],
        )
        desired_el, achieved_el = (
            dish_proxy.desiredPointing[2],
            dish_proxy.achievedPointing[2],
        )
        az_close_enough = abs(desired_az - achieved_az) <= TOL
        el_close_enough = abs(desired_el - achieved_el) <= TOL
        dish_far_from_requested_position = not (
            az_close_enough and el_close_enough
        )

        if future_time < time.time():
            break


@then("the actual azimuth should be almost equal to the desired azimuth")
def check_azimuth_position(dish_proxy):
    # pylint: disable=missing-function-docstring
    desired_az, achieved_az = (
        dish_proxy.desiredPointing[1],
        dish_proxy.achievedPointing[1],
    )
    assert_that(achieved_az).is_close_to(desired_az, 0.1)
    LOGGER.info("dish_master device arrived at the requested azimuth position")


@then("the actual elevation should be almost equal to the desired elevation")
def check_elevation_position(dish_proxy):
    # pylint: disable=missing-function-docstring
    desired_el, achieved_el = (
        dish_proxy.desiredPointing[2],
        dish_proxy.achievedPointing[2],
    )
    assert_that(achieved_el).is_close_to(desired_el, 0.1)
    LOGGER.info(
        "dish_master device arrived at the requested elevation position"
    )


@then(
    parsers.parse(
        "the pointingState should transition to {final_pt_state} on target"
    )
)
def check_pointing_state_on_target(dish_proxy, final_pt_state):
    # pylint: disable=missing-function-docstring
    current_pointing_state = retrieve_attr_value(dish_proxy, "pointingState")
    assert_that(current_pointing_state).is_equal_to(final_pt_state)
    LOGGER.info(f"dish master current pointingState: {current_pointing_state}")
