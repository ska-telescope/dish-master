"""
BDD tests for dish master stow
XTP-3090
"""

import logging
import time

import pytest
from assertpy import assert_that
from pytest_bdd import given, parsers, scenario, then, when
from tango import DeviceProxy

from tests.acceptance.utils import request_mode_change, retrieve_attr_value

LOGGER = logging.getLogger(__name__)

# elevation drive rate, TOL are from the dish behaviour in ska-tmc
# Use elevation to estimate time expected to arrive on target since
# it moves slower
ELEV_DRIVE_MAX_RATE = 1.0
TOL = 5e-3


@pytest.fixture(scope="module", name="initial_az")
def fixture_initial_az():
    # pylint: disable=missing-function-docstring

    # to be used in stow request
    return {"az": 0}


@pytest.fixture(scope="module", name="dish_proxy")
def fixture_dish_proxy():
    # pylint: disable=missing-function-docstring
    return DeviceProxy("mid_d0001/elt/master")


@pytest.fixture(autouse=True, scope="module")
def restore_dish_state(request, dish_proxy):
    """A teardown function which will ensure that the dish is restored to
    STANDBY state
    """

    def put_dish_in_standby_fp_mode():
        LOGGER.info("Restoring dish_master to STANDBY state")
        request_mode_change(dish_proxy, "STANDBY_FP")

    request.addfinalizer(put_dish_in_standby_fp_mode)


@scenario("stow-request.feature", "Test dish stow request")
def test_stow_command():
    # pylint: disable=missing-function-docstring
    pass


@given("dish master reports any allowed dishMode")
def dish_reports_any_dish_mode(dish_proxy):
    # pylint: disable=missing-function-docstring
    allowed_dish_modes = [
        "OFF",
        "STARTUP",
        "SHUTDOWN",
        "STANDBY_LP",
        "STANDBY_FP",
        "MAINTENANCE",
        "CONFIG",
        "OPERATE",
        "STOW",
    ]

    current_dish_mode = retrieve_attr_value(dish_proxy, "dishMode")
    assert_that(allowed_dish_modes).contains(current_dish_mode)
    LOGGER.info(f"dish_master initial dishMode: {current_dish_mode}")


@when("I execute a stow command")
def set_stow_mode(dish_proxy, initial_az):
    # pylint: disable=missing-function-docstring
    initial_az["az"] = dish_proxy.achievedPointing[1]
    initial_el = dish_proxy.achievedPointing[2]
    LOGGER.info(f"dish_master initial azimuth: {initial_az['az']}")
    LOGGER.info(f"dish_master initial elevation: {initial_el}")
    request_mode_change(dish_proxy, "STOW")
    LOGGER.info("dish_master requested dishMode: STOW")


@then("the dishMode should report STOW")
def check_dish_mode(dish_proxy):
    # pylint: disable=missing-function-docstring
    LOGGER.info("Waiting for dish_master dishMode to report STOW")
    current_dish_mode = retrieve_attr_value(dish_proxy, "dishMode")
    assert_that(current_dish_mode).is_equal_to("STOW")
    LOGGER.info(f"dish_master current dishMode: {current_dish_mode}")


@then(
    parsers.parse("the elevation should be almost equal to {stow_position:g}")
)
def check_dish_elevation(dish_proxy, stow_position):
    # pylint: disable=missing-function-docstring
    el_delta = abs(stow_position - dish_proxy.desiredPointing[2])
    expected_time_to_move = el_delta / ELEV_DRIVE_MAX_RATE
    # add an arbitrary 10s el time tolerance
    future = time.time() + expected_time_to_move + 10
    dish_far_from_stow_position = True

    while dish_far_from_stow_position:
        now = time.time()
        current_el = dish_proxy.desiredPointing[2]
        dish_far_from_stow_position = not stow_position - current_el <= TOL
        # sleep to avoid using full CPU resources
        # while waiting to arrive on target
        time.sleep(1)
        if future < now:
            break

    current_el = dish_proxy.achievedPointing[2]
    assert_that(current_el).is_close_to(stow_position, 0.1)
    LOGGER.info(f"dish_master elevation: {current_el}")


@then("the azimuth should remain in the same position")
def check_dish_azimuth(dish_proxy, initial_az):
    # pylint: disable=missing-function-docstring
    current_az = dish_proxy.achievedPointing[1]
    assert_that(current_az).is_equal_to(initial_az["az"])
    LOGGER.info(f"dish_master azimuth: {current_az}")


@then(parsers.parse("the pointingState should be {final_pt_state}"))
def check_pointing_state_after_stow(dish_proxy, final_pt_state):
    # pylint: disable=missing-function-docstring
    current_pointing_state = retrieve_attr_value(dish_proxy, "pointingState")
    assert_that(current_pointing_state).is_equal_to(final_pt_state)
    LOGGER.info(f"dish master current pointingState: {current_pointing_state}")


@then(parsers.parse("the dish state should be {dish_state}"))
def check_dish_state_after_stow(dish_proxy, dish_state):
    # pylint: disable=missing-function-docstring
    current_dish_state = retrieve_attr_value(dish_proxy, "State")
    assert_that(current_dish_state).is_equal_to(dish_state)
    LOGGER.info(f"dish master State: {dish_state}")
