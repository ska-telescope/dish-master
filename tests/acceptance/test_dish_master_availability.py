import pytest
import tango


@pytest.mark.acceptance
@pytest.mark.SKA_mid
@pytest.mark.parametrize("dish_number", ["0001", "0002", "0003", "0004"])
def test_dishes_are_available(dish_number):
    """Test that the 4 dishes we expect are available"""
    dsh_proxy = tango.DeviceProxy(f"mid_d{dish_number}/elt/master")
    assert isinstance(dsh_proxy.ping(), int)
    assert dsh_proxy.State() == tango.DevState.STANDBY
    assert dsh_proxy.dishMode.name == "STANDBY_LP"
    assert dsh_proxy.pointingState.name == "NONE"
