"""
BDD tests for dish master mode and pointing state transitions
XTP-813
"""

import logging

import pytest
from assertpy import assert_that
from pytest_bdd import given, scenario, then, when
from pytest_bdd.parsers import parse
from tango import DeviceProxy

from tests.acceptance.utils import (
    dish_mode_pre_condition,
    request_mode_change,
    retrieve_attr_value,
)

LOGGER = logging.getLogger(__name__)

# to be used in teardown
device_proxies = {}


@pytest.fixture(autouse=True, scope="module")
def restore_dish_state(request):
    """A teardown function which will ensure that all dishes used
    in the test are restored to STANDBY state
    """

    def put_dish_in_standby_fp_mode():
        LOGGER.info("Restoring all dishes to STANDBY state")
        for _, dp in device_proxies.items():
            request_mode_change(dp, "STANDBY_FP")

    request.addfinalizer(put_dish_in_standby_fp_mode)


@scenario(
    "mode-transitions.feature",
    "Test dish master dishMode and pointingState transitions",
)
def test_mode_and_pointing_state_transitions():
    # pylint: disable=missing-function-docstring
    pass


@given(parse("{dish_master} reports {initial_dish_mode} dish mode"))
def device_proxy(dish_master, initial_dish_mode):
    # pylint: disable=missing-function-docstring
    # update the device_proxies collection for teardown
    if dish_master not in device_proxies:
        dev_proxy = DeviceProxy(dish_master)
        device_proxies[dish_master] = dev_proxy
    dish_mode_pre_condition(device_proxies[dish_master], initial_dish_mode)
    LOGGER.info(f"{dish_master} initial dishMode: {initial_dish_mode}")


@when(parse("I request {dish_master} transition to {desired_dish_mode}"))
def set_dish_mode(dish_master, desired_dish_mode):
    # pylint: disable=missing-function-docstring
    request_mode_change(device_proxies[dish_master], desired_dish_mode)
    LOGGER.info(f"{dish_master} requested {desired_dish_mode} mode")


@then(parse("{dish_master} dishMode reports {desired_dish_mode}"))
def check_dish_mode(dish_master, desired_dish_mode):
    # pylint: disable=missing-function-docstring
    current_dish_mode = retrieve_attr_value(
        device_proxies[dish_master], "dishMode"
    )
    assert_that(current_dish_mode).is_equal_to(desired_dish_mode)
    LOGGER.info(f"{dish_master} dishMode: {current_dish_mode}")


@then(parse("{dish_master} pointingState reports {pointing_state}"))
def check_pointing_state(dish_master, pointing_state):
    # pylint: disable=missing-function-docstring
    current_pointing_state = retrieve_attr_value(
        device_proxies[dish_master], "pointingState"
    )
    assert_that(current_pointing_state).is_equal_to(pointing_state)
    LOGGER.info(f"{dish_master} pointingState: {current_pointing_state}")


@then(parse("{dish_master} state reports {dish_state}"))
def check_master_device_state(dish_master, dish_state):
    # pylint: disable=missing-function-docstring
    current_dish_state = retrieve_attr_value(
        device_proxies[dish_master], "State"
    )
    assert_that(current_dish_state).is_equal_to(dish_state)
    LOGGER.info(f"{dish_master} state: {current_dish_state}")
