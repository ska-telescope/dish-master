Feature: Dish master pointing position acceptance tests

	@SKA_mid @acceptance
	Scenario: Test dish pointing request
		Given dish master reports OPERATE dishMode
		And dish master reports READY pointingState
		When I request the azimuth and elevation to move by 10 degrees each
		Then the actual azimuth should be almost equal to the desired azimuth
		And the actual elevation should be almost equal to the desired elevation
		And the pointingState should transition to TRACK on target
