DishMaster Simulator
====================

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-sim-dishmaster/badge/?version=latest)](https://developer.skao.int/projects/ska-sim-dishmaster/en/latest/?badge=latest)



This package provides the ability to run simulators for the SKA Dish Master TANGO device servers.

Usage
-----
Start your device server in any
terminal running:

`DishMasterDS instance_name`

Requirements
------------

The system used for development needs to have Python 3, `pip` and `poetry` installed.


Installation
------------

Install skuid client package as above.

```bash
$ pip install --extra-index-url=https://artefact.skao.int/repository/pypi-all/simple ska-sim-dishmaster
```

Install the requirements.

```bash
$ poetry install
```

Testing
-------

* Put tests into the `tests` folder
* Use [PyTest](https://pytest.org) as the testing framework
  - Reference: [PyTest introduction](http://pythontesting.net/framework/pytest/pytest-introduction/)
* Running tests:
  - Install `tox`
  - `tox -e test`
* Running the test creates the `build/reports/htmlcov` folder
    - Inside this folder a rundown of the issues found will be accessible using the `index.html` file
* All the tests should pass before merging the code

 Code analysis
 -------------
 * Use [Pylint](https://www.pylint.org) as the code analysis framework
 * By default it uses the [PEP8 style guide](https://www.python.org/dev/peps/pep-0008/)
 * Use `tox -e lint`
 * Code analysis should only raise document related warnings (i.e. `#FIXME` comments) before merging the code

Writing documentation
 --------------------
 * The documentation generator for this project is derived from SKA's [SKA Developer Portal repository](https://github.com/ska-telescope/developer.skatelescope.org)
 * The documentation can be edited under `./docs/src`

 * In order to build the documentation for this specific project, execute the following under `./docs` ensure `tox` has been installed
    ```bash
    $ tox -e docs
    ```
* The documentation can then be consulted by opening the file `./docs/build/html/index.html`
