###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

Version 2.1.0
*************
- Upgraded chart dependency for ska-tango-util (v0.4.5) and ska-tango-base (v0.4.4)
- Updated makefile for pipeline machinery
- Added requirement to include yaml for ReadTheDocs

Version 2.0.3
*************
Upgraded dependency package, tango-simlib, to v0.9.4.

Version 2.0.2
*************
No functionality changes, added acceptance tests

Version 2.0.1
*************
No functionality changes, added charts

Version 2.0.0
*************
The first release of the dishmaster simulator supports the
following:

- Repo structure and contents conforms to standard
- Dishmaster simulator can be run
- Dishmaster is included in docker image
- Dishmaster is packaged in helm chart and published to CAR
