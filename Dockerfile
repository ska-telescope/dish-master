FROM artefact.skao.int/ska-tango-images-pytango-builder:9.3.34 AS buildenv
FROM artefact.skao.int/ska-tango-images-pytango-runtime:9.3.21 AS runtime

USER root

COPY pyproject.toml poetry.lock* ./

# install runtime dependencies and the app
RUN poetry config virtualenvs.create false && poetry install

USER tango
